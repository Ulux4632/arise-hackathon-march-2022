This directory contains the following Darwin Core Archive data sets:

* Limburg: <https://www.doi.org/10.15468/dl.v2v7p9>
* Veluwe: <https://www.doi.org/10.15468/dl.uzwbk6>
* Meijendel: <https://www.doi.org/10.15468/dl.ykxhcq>
