
Western Mediterranean Wetlands Bird Dataset
=====

The Western Mediterranean Wetlands Bird Dataset contains 5795 audio clips for 20 bird species typically found in a wetland in Spain. The audio files and their detailed annotations are provided, as well as a Mel spectrogram version of the dataset, where each image corresponds to 1-second window of the original audio, resulting in a total of 17,536 spectrogram images.

* Download the data (zip file "audio_files.zip"): https://zenodo.org/record/5093173 
* Paper: not published yet.
* Code (for deep learning recognition): https://github.com/jogomez97/BirdMLClassification

